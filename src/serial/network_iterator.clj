(ns serial.network-iterator
  (:require [network-traversal :as network]
            experts))

(def path-memo (atom nil))

(defn next-node[]
  (if-let[[node-id path] @path-memo]
    (let [node (experts/node node-id)]
     (reset! path-memo (network/move path (:connections node)))
    node)))
      
(defn iterator[root-id]
  (reset! path-memo [root-id (network/path)]) 
  (take-while some? (repeatedly next-node)))
