(ns run
  (:require [clojure.core.async :as async]))

(def expertise {})
(def chan-reqs (async/chan 100 (map network-member)))
(defn add-expertise[e plants]
  (println "adding expertise " plants)
  plants)

(defn harvester[expert-chan]
  (async/go-loop [c expert-chan e expertise]
          (if-let [expert (async/<! expert-chan)]
            (recur (async/onto-chan c (:folowing expert)) (add-expertise e (:plants expert)))
            expertise)))

(defn -main[]
  (let [h (harvester chan-reqs)]
  (async/>!! chan-reqs 0)
  (println (async/<!! h))))
