(ns network.iterator)

(defprotocol NetworkIterator
  (current[this] "returns current node")
  (move[this nodes] "advances iterator and adds more nodes to iterate"))


