(ns network.iterators.bf-concat-dedupe
  (:use network.iterator))

(defn- do-move
  "Maps [network-path new-nodes] to [next-unexplored-node, updated-path]
  Returns nil when all nodes explored."
  [[traversed untraversed] new-nodes]
    (if-let[new-untraversed (seq (remove traversed (concat untraversed new-nodes)))]
      (let [[next-node & remaining] new-untraversed
            next-path [(conj traversed next-node) remaining]]
         {:curr next-node :path next-path})))

(defrecord Cursor [curr path]
  NetworkIterator
  (current[this] (:curr this))
  (move[this nodes] (map->Cursor (do-move (:path this) nodes))))

(defn cursor[root]
  (->Cursor root [(set [root]) []]))


