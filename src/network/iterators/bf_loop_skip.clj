(ns network.iterators.bf-loop-skip
  (:use network.iterator))

(defn cursor[root]
  [(set [root]) []])

(defn- skip-traversed[traversed untraversed] 
  (if-let [[node & remaining] (seq untraversed)]
    (if (traversed node)
      (recur traversed remaining)
      [node remaining])))

(defn- do-move
  "Maps [path new-nodes] to [next-unexplored-node, updated-path]
  Returns nil when all nodes explored."
[[traversed untraversed] new-nodes]
  (if-let [[next-node remaining] (skip-traversed traversed (concat untraversed new-nodes))]
    {:curr next-node 
     :path [(conj traversed next-node) remaining]}))

(defrecord Cursor [curr path]
  NetworkIterator
  (current[this] (:curr this))
  (move[this nodes] (map->Cursor (do-move (:path this) nodes))))

(defn cursor[root]
  (->Cursor root [(set [root]) []]))

