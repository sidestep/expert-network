(ns network.iterators.bf-single-queue
  (:use network.iterator))

(defn- append-dedupe[vec1 coll2]
  "Appends to vector values from coll that are not already present in vector."
  (into vec1 (remove (set vec1)) (distinct coll2)))

(defn do-move
  "Maps [cursor new-nodes] to [next-node, next-cursor]
  Returns nil when all nodes explored."
  [idx path new-nodes]
    (let [new-path (append-dedupe path new-nodes)]
      (if (< idx (count new-path)) 
       {:idx (inc idx)
        :path new-path})))

(defrecord Cursor [idx path]
  NetworkIterator
  (current[this] (get (:path this) idx))
  (move[this nodes] (map->Cursor (do-move (:idx this) (:path this) nodes))))

(defn cursor[root]
  (->Cursor 0 [root]))
