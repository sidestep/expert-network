(ns kv-service
  (:require [cheshire.core :as json])
  (:use [compojure.core :only [defroutes GET]]
        [org.httpkit.server :only [run-server]]))

(declare store)

(defn resp[req]
  (assoc
    {:status 200
     :headers {"Content-Type" "application/json"}}
     :body (json/generate-string (store (-> req :params :key)))))

(defroutes all-routes
  (GET "/:key" [] resp))

(defn start[dict]
  (def store dict)
  (let [port 8080]
    (run-server all-routes {:port port})))


