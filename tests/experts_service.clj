(ns tests.experts-service
  (:require [kv-service :as server]
            [cheshire.core :as json]
            [clojure.spec.alpha :as spec]
            [org.httpkit.client :as client])
  (:use clojure.test))

(spec/def ::id some?)
(spec/def ::plants (spec/coll-of string? :distinct true :min-count 1))
(spec/def ::connections (spec/nilable (spec/coll-of some? :distinct true)))
(spec/def ::expert (spec/keys :req-un [::id ::plants ::connections]))

(defn mock-expert[id]
  {
     :id id 
     :plants (random-sample 0.5 ["Tomatoes" "Sugarcane" "Grapes" "Apples" "Onions" "Cucumbers" "Garlic" "Bananas" "Mangos" "Watermelons" "Olives" "Tea"])
     :connections (random-sample 0.1 (range 100))
  })

(def stop-srv (server/start mock-expert))

(use-fixtures :once (fn[tests]
                      (tests)
                      (stop-srv)))

(defn http-get-expert[id]
  (let [{:keys [body status]} @(client/get (str "http://localhost:8080/" id))]
    (json/parse-string body true)))

(deftest expert-with-empty-plants-not-valid[]
  (let [expert (mock-expert 1)]
    (is (spec/valid? ::expert expert))
    (is (not (spec/valid? ::expert (assoc expert :plants []))))))

(deftest expert-with-empty-connections-valid[]
  (let [expert (mock-expert 1)]
    (is (spec/valid? ::expert expert))
    (is (spec/valid? ::expert (assoc expert :connections nil)))))

(deftest server-gets-all-experts[]
  (let [responses (take 10 (repeatedly #(http-get-expert (rand-int 10000))))]
    (is (every? #(spec/valid? ::expert %) responses))))
