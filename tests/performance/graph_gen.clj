(require '[clojure.edn :as edn])

(let [[cnt degree] (doall (map edn/read-string *command-line-args*))
      nodes (range cnt)
      graph (into {} (for [n nodes] [n (random-sample degree nodes)]))]
  (binding [*print-meta* true]
      (prn (with-meta graph {:name (str cnt " nodes " degree " degree")}))))

