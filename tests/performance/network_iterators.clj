(ns tests.performance.network-iterators
  (:require network.iterator 
            [clojure.edn :as edn]
            [network.iterators 
             [bf-concat-dedupe :as bf-concat-dedupe]
             [bf-loop-skip :as bf-loop-skip]
             [bf-single-queue :as bf-single-queue]]))

(defn traverse[cursor graph] 
  (loop [c cursor]
    (if-let [node (network.iterator/current c)]
      (recur (network.iterator/move c (graph node))))))

(defmacro mtime [expr]
  `(let [start# (. System (nanoTime))]
     ~expr
     (/ (double (- (. System (nanoTime)) start#)) 1000000.0)))

(def graph-files (.listFiles (clojure.java.io/file "tests/performance/graphs")))
(def cursors [(with-meta (bf-concat-dedupe/cursor 0) {:name "concat-dedupe"})
              (with-meta (bf-loop-skip/cursor 0)     {:name "loop-skip    "})
              (with-meta (bf-single-queue/cursor 0)  {:name "single-queue "})])

(doseq [gfile graph-files]
 (let [graph (edn/read-string (slurp gfile))]
   (println "--- " ((meta graph) :name) " ---")
   (doseq [cursor cursors]
     (println ((meta cursor) :name) (mtime (traverse cursor graph))))))
