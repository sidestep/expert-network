(ns tests.network-traversal
  (:use clojure.test)
  (:require network.iterator)
  (:require
    [network.iterators 
     [bf-concat-dedupe :as bf-concat-dedupe] 
     [bf-loop-skip :as bf-loop-skip]
     [bf-single-queue :as bf-single-queue]]))

(defn graph[nodes] 
  (apply hash-map nodes)) 

(defn traverse[cursor graph] 
  (loop [c cursor acc []]
    (if-let [node (network.iterator/current c)]
      (recur (network.iterator/move c (graph node)) (conj acc node))
      acc)))

(defn traverse-with-all-iterators[graph]
   (doall 
     (for [cursor [(bf-concat-dedupe/cursor 0) (bf-loop-skip/cursor 0) (bf-single-queue/cursor 0)]]
       (traverse cursor graph))))

(deftest empty-graph-traversed[]
  (let [g (graph [])]
    ;root node '0' is still traversed
    (is (every? #(= % [0]) (traverse-with-all-iterators g)))))

(deftest single-node-graph-traversed[]
  (let [g (graph [0 []])]
    (is (every? #(= % [0]) (traverse-with-all-iterators g)))))
        
(deftest all-nodes-traversed[] 
  (let [g (graph [0 [1 2 4] 1 [5 3]])]
    (is (every? #(= % [0 1 2 4 5 3]) (traverse-with-all-iterators g)))))

(deftest cycle-graph-traversed[]
  (let [g (graph [0 [1] 1 [0]])]
    (is (every? #(= % [0 1]) (traverse-with-all-iterators g)))))

(deftest fully-connected-graph-traversed[]
  (let [g (graph [0 [1 2 3 4 5] 1 [0 2 3 4 5] 3 [0 1 2 4 5] 4 [0 1 2 3 5] 5 [0 1 2 3 4]])]
    (is (every? #(= % [0 1 2 3 4 5]) (traverse-with-all-iterators g)))))

(deftest disconnected-nodes-not-traversed[] 
  (let [g (graph [0 [1 2 3] 1 [2 3] 3 [0 1] 10 [11 12 13]])] 
    (is (every? #(= % [0 1 2 3]) (traverse-with-all-iterators g)))))
 
(deftest nodes-with-single-connection-traversed[]
  (let [g (graph [0 [1 2 3] 1 [2 3] 3 [0 1 10] 10 [11 12 13]])] 
    (is (every? #(= % [0 1 2 3 10 11 12 13]) (traverse-with-all-iterators g)))))
